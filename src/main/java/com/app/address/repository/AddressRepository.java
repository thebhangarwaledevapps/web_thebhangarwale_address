package com.app.address.repository;

import java.util.ArrayList;
import java.util.Optional;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.app.address.entity.Address;

public interface AddressRepository extends CrudRepository<Address, Long>{

	@Query("FROM Address WHERE customerId = ?1")
	public Optional<ArrayList<Address>> findAddressesByCustomerId(String customerId);
	
	@Query("FROM Address WHERE customerId =:customerId AND addressId =:addressId")
	Optional<Address> findAddressByCustomerIdAndAddressId(@Param("customerId")Long customerId,@Param("addressId")Long addressId);

	
	
}

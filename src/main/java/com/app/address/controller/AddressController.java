package com.app.address.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.app.address.entity.Response;
import com.app.address.entity.AddressResponse;
import com.app.address.exception.InvalidAddressException;
import com.app.address.exception.InvalidPincodeException;
import com.app.address.exception.NotBelongCityException;
import com.app.address.service.AddressService;
import com.app.address.util.MessageUtils;

@RestController
@RequestMapping("/address")
public class AddressController {
	
	@Autowired
	private AddressService addressService;
	
	@RequestMapping(value = "/getAddress", method = RequestMethod.GET)
	public ResponseEntity<AddressResponse> getAddress(@RequestParam String customerId) {
		AddressResponse addressResponse;
		ResponseEntity<AddressResponse> responseEntity = null;
		try {
			addressResponse = addressService.getAddress(customerId);
			responseEntity = new ResponseEntity<AddressResponse>(addressResponse, HttpStatus.OK);
		} catch (Exception e) {
			addressResponse = new AddressResponse(MessageUtils.SOMETHING_WENT_WRONG);
			responseEntity = new ResponseEntity<AddressResponse>(addressResponse, HttpStatus.INTERNAL_SERVER_ERROR);
		} 
		return responseEntity;
	}
	
	@RequestMapping(value = "/saveAddressWithGoogleMap", method = RequestMethod.GET)
	public ResponseEntity<Response> saveAddressWithGoogleMap(
			@RequestParam(name ="customerId",required = false) String customerId,
			@RequestParam(name ="address",required = false) String address,
			@RequestParam(name ="pinCode",required = false) String pinCode,
			@RequestParam(name ="landMark",required = false) String landMark,
			@RequestParam(name ="latitude",required = false) Double latitude,
			@RequestParam(name ="longitude",required = false) Double longitude) {
		Response response;
		ResponseEntity<Response> responseEntity = null;
		try {
			response = addressService.saveAddressWithGoogleMap(customerId,address,pinCode,landMark,latitude,longitude);
			responseEntity = new ResponseEntity<Response>(response, HttpStatus.OK);
		}catch (InvalidAddressException e) {
			response = new Response(e.getMessage()){};
			responseEntity = new ResponseEntity<Response>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (InvalidPincodeException e) {
			response = new Response(e.getMessage()){};
			responseEntity = new ResponseEntity<Response>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (NotBelongCityException e) {
			response = new Response(e.getMessage()){};
			responseEntity = new ResponseEntity<Response>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (Exception e) {
			response = new Response(MessageUtils.SOMETHING_WENT_WRONG){};
			responseEntity = new ResponseEntity<Response>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		} 
		return responseEntity;
	}
	
	@RequestMapping(value = "/saveAddressManually", method = RequestMethod.GET)
	public ResponseEntity<Response> saveAddressManually(
			@RequestParam(name ="customerId") String customerId,
			@RequestParam(name ="address") String address,
			@RequestParam(name ="pinCode") String pinCode,
			@RequestParam(name ="landMark") String landMark) {
		Response response;
		ResponseEntity<Response> responseEntity = null;
		try {
			response = addressService.saveAddressManually(customerId,address,pinCode,landMark);
			responseEntity = new ResponseEntity<Response>(response, HttpStatus.OK);
		}catch (InvalidAddressException e) {
			response = new Response(e.getMessage()){};
			responseEntity = new ResponseEntity<Response>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (InvalidPincodeException e) {
			response = new Response(e.getMessage()){};
			responseEntity = new ResponseEntity<Response>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (NotBelongCityException e) {
			response = new Response(e.getMessage()){};
			responseEntity = new ResponseEntity<Response>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (Exception e) {
			response = new Response(MessageUtils.SOMETHING_WENT_WRONG){};
			responseEntity = new ResponseEntity<Response>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		} 
		return responseEntity;
	}
		
	
	/*@SuppressWarnings("finally")
	@RequestMapping(value = "/updateAndReturnAddress", method = RequestMethod.POST)
	public ResponseEntity<Response> updateAndReturnAddress(@RequestBody Address address) {
		Response response = null;
		ResponseEntity<Response> responseEntity = null;		
		try {
			response = addressService.updateAndReturnAddress(address);
			responseEntity = new ResponseEntity<Response>(response, HttpStatus.OK);
		} catch (InvalidRequestException e) {
			response = new AddressResponse(e.getMessage());
			responseEntity = new ResponseEntity<Response>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (InvalidAddressIdException e) {
			response = new AddressResponse(e.getMessage());
			responseEntity = new ResponseEntity<Response>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (InvalidCustomerIdException e) {
			response = new AddressResponse(e.getMessage());
			responseEntity = new ResponseEntity<Response>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (InvalidAddressException e) {
			response = new AddressResponse(e.getMessage());
			responseEntity = new ResponseEntity<Response>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (InvalidPincodeException e) {
			response = new AddressResponse(e.getMessage());
			responseEntity = new ResponseEntity<Response>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (NotBelongCityException e) {
			response = new AddressResponse(e.getMessage());
			responseEntity = new ResponseEntity<Response>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (SaveAddressException e) {
			response = new AddressResponse(e.getMessage());
			responseEntity = new ResponseEntity<Response>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (Exception e) {
			response = new AddressResponse(MessageUtils.SOMETHING_WENT_WRONG);
			responseEntity = new ResponseEntity<Response>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		} finally {
			return responseEntity;
		}
	}
	
	
	@SuppressWarnings("finally")
	@RequestMapping(value = "/deleteAndReturnAddress", method = RequestMethod.POST)
	public ResponseEntity<AddressResponse> deleteAndReturnAddress(@RequestBody ArrayList<Address> address) {
		AddressResponse addressResponse;
		ResponseEntity<AddressResponse> responseEntity = null;
		try {
			addressResponse = addressService.deleteAndReturnAddress(address);
			responseEntity = new ResponseEntity<AddressResponse>(addressResponse, HttpStatus.OK);
		} catch (InvalidRequestException e) {
			addressResponse = new AddressResponse(e.getMessage());
			responseEntity = new ResponseEntity<AddressResponse>(addressResponse, HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (InvalidAddressesException e) {
			addressResponse = new AddressResponse(e.getMessage());
			responseEntity = new ResponseEntity<AddressResponse>(addressResponse, HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (InvalidAddressIdException e) {
			addressResponse = new AddressResponse(e.getMessage());
			responseEntity = new ResponseEntity<AddressResponse>(addressResponse, HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (Exception e) {
			addressResponse = new AddressResponse(MessageUtils.SOMETHING_WENT_WRONG);
			responseEntity = new ResponseEntity<AddressResponse>(addressResponse, HttpStatus.INTERNAL_SERVER_ERROR);
		} finally {
			return responseEntity;
		}
	}
	
	
	@SuppressWarnings("finally")
	@RequestMapping(value = "/getAddressByCustomerIdAndAddressIdForDeveloper", method = RequestMethod.GET)
	public ResponseEntity<AddressForDeveloperResponse> getAddressByCustomerIdAndAddressIdForDeveloper(@RequestParam Long customerId,@RequestParam Long addressId) {
		AddressForDeveloperResponse addressForDeveloperResponse;
		ResponseEntity<AddressForDeveloperResponse> responseEntity = null;
		try {
			addressForDeveloperResponse = addressService.getAddressByCustomerIdAndAddressIdForDeveloper(customerId, addressId);
			responseEntity = new ResponseEntity<AddressForDeveloperResponse>(addressForDeveloperResponse, HttpStatus.OK);
		} catch (InvalidCustomerIdForDeveloperException e) {
			addressForDeveloperResponse = new AddressForDeveloperResponse(e.getMessage());
			responseEntity = new ResponseEntity<AddressForDeveloperResponse>(addressForDeveloperResponse, HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (InvalidAddressIdForDeveloperException e) {
			addressForDeveloperResponse = new AddressForDeveloperResponse(e.getMessage());
			responseEntity = new ResponseEntity<AddressForDeveloperResponse>(addressForDeveloperResponse, HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (AddressOptionalNullForDeveloperException e) {
			addressForDeveloperResponse = new AddressForDeveloperResponse(e.getMessage());
			responseEntity = new ResponseEntity<AddressForDeveloperResponse>(addressForDeveloperResponse, HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (AddressOptionalNotPresentForDeveloperException e) {
			addressForDeveloperResponse = new AddressForDeveloperResponse(e.getMessage());
			responseEntity = new ResponseEntity<AddressForDeveloperResponse>(addressForDeveloperResponse, HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (AddressNullForDeveloperException e) {
			addressForDeveloperResponse = new AddressForDeveloperResponse(e.getMessage());
			responseEntity = new ResponseEntity<AddressForDeveloperResponse>(addressForDeveloperResponse, HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (Exception e) {
			addressForDeveloperResponse = new AddressForDeveloperResponse(MessageUtils.SOMETHING_WENT_WRONG);
			responseEntity = new ResponseEntity<AddressForDeveloperResponse>(addressForDeveloperResponse, HttpStatus.INTERNAL_SERVER_ERROR);
		} finally {
			return responseEntity;
		}
		
	}*/
	
	
	

}

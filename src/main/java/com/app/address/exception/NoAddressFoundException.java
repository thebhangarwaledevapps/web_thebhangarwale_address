package com.app.address.exception;

import com.app.address.util.MessageUtils;

public class NoAddressFoundException extends RuntimeException{
	
	public NoAddressFoundException(String functionName) {
		super(MessageUtils.NO_ADDRESSES_FOUND);
	}


}

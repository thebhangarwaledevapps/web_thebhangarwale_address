package com.app.address.exception;

import com.app.address.util.MessageUtils;

public class NotBelongCityException extends RuntimeException{
	
	public NotBelongCityException() {
		super(MessageUtils.NOT_BELONG_CITY);
	}


}

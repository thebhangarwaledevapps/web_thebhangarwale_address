package com.app.address.exception;

import com.app.address.util.MessageUtils;

public class InvalidPincodeException extends RuntimeException{
	
	public InvalidPincodeException() {
		super(MessageUtils.PINCODE_IS_INVALID);
	}


}
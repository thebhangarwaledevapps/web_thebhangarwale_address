package com.app.address.exception;

import com.app.address.util.MessageUtils;

public class InvalidAddressException extends RuntimeException{
	
	public InvalidAddressException() {
		super(MessageUtils.ADDRESS_IS_INVALID);
	}


}
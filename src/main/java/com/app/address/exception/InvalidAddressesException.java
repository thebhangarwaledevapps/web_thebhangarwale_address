package com.app.address.exception;

import com.app.address.util.MessageUtils;

public class InvalidAddressesException extends RuntimeException{
	
	public InvalidAddressesException(String functionName) {
		super(MessageUtils.NO_ADDRESSES_FOUND);
	}


}

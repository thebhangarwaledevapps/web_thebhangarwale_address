package com.app.address.entity;

import java.io.Serializable;

public class PincodeMainArrayObject implements Serializable{

	private PincodeMainObject pincodeMainObject;
	
	public PincodeMainArrayObject() {
		super();
	}

	public PincodeMainArrayObject(PincodeMainObject pincodeMainObject) {
		super();
		this.pincodeMainObject = pincodeMainObject;
	}

	public PincodeMainObject getPincodeMainObject() {
		return pincodeMainObject;
	}

	public void setPincodeMainObject(PincodeMainObject pincodeMainObject) {
		this.pincodeMainObject = pincodeMainObject;
	}
	
}

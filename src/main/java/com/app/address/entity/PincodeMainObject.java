package com.app.address.entity;

import java.io.Serializable;
import java.util.ArrayList;

public class PincodeMainObject implements Serializable{
	
	private String Message;
	private String Status;
	private ArrayList<PostOffice> PostOffice;
	
	public PincodeMainObject() {
		super();
	}

	public PincodeMainObject(String message, String status, ArrayList<PostOffice> postOffice) {
		super();
		Message = message;
		Status = status;
		PostOffice = postOffice;
	}

	public String getMessage() {
		return Message;
	}

	public void setMessage(String message) {
		Message = message;
	}

	public String getStatus() {
		return Status;
	}

	public void setStatus(String status) {
		Status = status;
	}

	public ArrayList<PostOffice> getPostOffice() {
		return PostOffice;
	}

	public void setPostOffice(ArrayList<PostOffice> postOffice) {
		PostOffice = postOffice;
	}
	
}

package com.app.address.entity;

import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotBlank;

@Entity
public class Address implements Serializable{

	@GeneratedValue(strategy = GenerationType.AUTO)
	@Id
	private Long addressId;
	@NotBlank
	private String customerId;
	@NotBlank
	private String address;
	@NotBlank
	private String pincode;
	@OneToOne(cascade = CascadeType.ALL,fetch = FetchType.EAGER,orphanRemoval = true)
	private LatLong latLong;
	private String landMark;
	
	public Address() {
		super();
	}

	public Address(@NotBlank String customerId, @NotBlank String address, @NotBlank String pincode,
			LatLong latLong, String landMark) {
		super();
		this.customerId = customerId;
		this.address = address;
		this.pincode = pincode;
		this.latLong = latLong;
		this.landMark = landMark;
	}

	public Long getAddressId() {
		return addressId;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPincode() {
		return pincode;
	}

	public void setPincode(String pincode) {
		this.pincode = pincode;
	}

	public LatLong getLatLong() {
		return latLong;
	}

	public void setLatLong(LatLong latLong) {
		this.latLong = latLong;
	}

	public String getLandMark() {
		return landMark;
	}

	public void setLandMark(String landMark) {
		this.landMark = landMark;
	}

}

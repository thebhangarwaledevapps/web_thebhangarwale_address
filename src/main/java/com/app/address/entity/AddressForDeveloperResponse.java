package com.app.address.entity;

public class AddressForDeveloperResponse extends Response{

	private Address address;
	
	public AddressForDeveloperResponse(String message) {
		super(message);
	}

	public AddressForDeveloperResponse(String message, Address address) {
		super(message);
		this.address = address;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	@Override
	public String toString() {
		return "AddressForDeveloperResponse [address=" + address + "]";
	}
	
}

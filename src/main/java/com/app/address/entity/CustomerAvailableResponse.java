package com.app.address.entity;

import java.io.Serializable;

public class CustomerAvailableResponse implements Serializable{

	private String message;
	private boolean isCustomerAvailable;
	
	public CustomerAvailableResponse() {}

	public CustomerAvailableResponse(String message, boolean isCustomerAvailable) {
		super();
		this.message = message;
		this.isCustomerAvailable = isCustomerAvailable;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public boolean isCustomerAvailable() {
		return isCustomerAvailable;
	}

	public void setCustomerAvailable(boolean isCustomerAvailable) {
		this.isCustomerAvailable = isCustomerAvailable;
	}
	
}

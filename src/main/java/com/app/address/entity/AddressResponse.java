package com.app.address.entity;

import java.util.ArrayList;

public class AddressResponse extends Response{

	private ArrayList<Address> addresses;
	
	public AddressResponse(String message) {
		super(message);
	}

	public AddressResponse(String message, ArrayList<Address> addresses) {
		super(message);
		this.addresses = addresses;
	}

	public ArrayList<Address> getAddresses() {
		return addresses;
	}

	public void setAddresses(ArrayList<Address> addresses) {
		this.addresses = addresses;
	}	
	
}

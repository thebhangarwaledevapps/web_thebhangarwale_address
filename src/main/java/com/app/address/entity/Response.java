package com.app.address.entity;

import java.io.Serializable;

public abstract class Response implements Serializable{

	protected String message;

	public Response(String message) {
		super();
		this.message = message;
	}

	public String getMessage() {
		return message;
	}
		
}

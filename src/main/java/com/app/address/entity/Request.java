package com.app.address.entity;

import java.io.Serializable;

public abstract class Request implements Serializable{

	protected long customerId;

	public Request(long customerId) {
		super();
		this.customerId = customerId;
	}

	public long getCustomerId() {
		return customerId;
	}	
	
}

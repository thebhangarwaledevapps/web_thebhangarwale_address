package com.app.address.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Entity
public class LatLong implements Serializable{

	@GeneratedValue(strategy = GenerationType.AUTO)
	@Id
	private long latLongId;
	@NotNull 
	private Double latitude;
	@NotNull  
	private Double longitude;
	
	public LatLong() {
		super();
	}

	public LatLong(@NotNull Double latitude, @NotNull Double longitude) {
		super();
		this.latitude = latitude;
		this.longitude = longitude;
	}

	public long getLatLongId() {
		return latLongId;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

}

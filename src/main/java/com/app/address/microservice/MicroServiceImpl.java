package com.app.address.microservice;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.app.address.entity.CustomerAvailableResponse;
import com.app.address.entity.PincodeMainObject;
import com.app.address.entity.PostOffice;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

@Service
public class MicroServiceImpl implements IMicroService {
	
	@Autowired
	RestTemplate rs;
	
	private final String CITY = "nagpur";
	
	@Override
	public boolean isPincodeNotBelongToExpectedCity(String pincode) {
		try {
			final String baseUrl = "https://api.postalpincode.in/pincode/"+pincode;
			ResponseEntity<String> responseEntity =new RestTemplate().getForEntity(baseUrl, String.class);
			Gson gson = new Gson();
			ArrayList<PincodeMainObject> pincodeMainArrayObject = gson.fromJson(responseEntity.getBody(),  new TypeToken<List<PincodeMainObject>>(){}.getType());
			List<PostOffice> postoffice = pincodeMainArrayObject.get(0).getPostOffice().stream().
					filter(postOffice->!postOffice.getDistrict().toLowerCase().equalsIgnoreCase(CITY))
					.collect(Collectors.toList());
			return !postoffice.isEmpty();
		} catch(Exception e) {
			System.out.println(e.getMessage());
			return true;
		}
	}

	@Override
	public boolean isCustomerAvailable(String customerId) {
		try {
			final MultiValueMap<String, String> params = new LinkedMultiValueMap<String, String>();
			params.add("customerId", customerId);
			final CustomerAvailableResponse customerAvailableResponse = rs.exchange(
					UriComponentsBuilder.newInstance().scheme("https").host("web-thebhangarwale-login").pathSegment("login")
							.pathSegment("isCustomerAvailable").queryParams(params).build().toString(),
					HttpMethod.GET, null, CustomerAvailableResponse.class, params).getBody();
			return customerAvailableResponse.isCustomerAvailable();
		} catch (Exception e) {
			return false;
		}	
	}

	@Override
	public PostOffice getCityDetail(String pincode) {
		try {
			final String baseUrl = "https://api.postalpincode.in/pincode/"+pincode;
			ResponseEntity<String> responseEntity =new RestTemplate().getForEntity(baseUrl, String.class);
			Gson gson = new Gson();
			ArrayList<PincodeMainObject> pincodeMainArrayObject = gson.fromJson(responseEntity.getBody(),  new TypeToken<List<PincodeMainObject>>(){}.getType());
			return pincodeMainArrayObject.get(0).getPostOffice().get(0);
		} catch(Exception e) {
			return null;
		}
	}

}

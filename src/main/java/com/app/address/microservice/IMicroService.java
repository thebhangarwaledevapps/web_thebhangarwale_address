package com.app.address.microservice;

import org.springframework.stereotype.Service;

import com.app.address.entity.PostOffice;

@Service
public interface IMicroService {
	
	boolean isPincodeNotBelongToExpectedCity(String pincode);
	
	boolean isCustomerAvailable(String customerId);
	
	PostOffice getCityDetail(String pincode);

}

package com.app.address.util;

public class MessageUtils {

	public static final String SOMETHING_WENT_WRONG = "Something went wrong";
	public static final String NO_ADDRESS_FOUND = "No address found";
	public static final String ADDRESS_IS_INVALID = "Enter valid address";
	public static final String PINCODE_IS_INVALID = "Enter valid pincode";
	public static final String NOT_BELONG_CITY = "Pincode do not belongs Nagpur";
	public static final String NO_ADDRESSES_FOUND = "No address are selected";
	public static final String SAVE_ADDRESS_SUCCESSFULLY = "Address saved successfully";
	public static final String UPDATE_ADDRESS_SUCCESSFULLY = "Address updated successfully";
	public static final String ADDRESS_ID_IS_INVALID = "Enter valid address id";
	public static final String CUSTOMER_ID_IS_INVALID = "Enter valid customer id";
		
}

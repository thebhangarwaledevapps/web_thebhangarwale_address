package com.app.address.util;

@org.springframework.stereotype.Service
public class UtilService implements Service{
		
	@Override
	public boolean isNotValidPincode(String otp) {
		if(otp==null) {
			return true;
		}
		else if(otp.isEmpty()) {
			return true;
		}
		return false;
	}
	
}

package com.app.address.service;

import java.util.ArrayList;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestParam;

import com.app.address.entity.Address;
import com.app.address.entity.AddressForDeveloperResponse;
import com.app.address.entity.AddressResponse;
import com.app.address.entity.LatLong;
import com.app.address.entity.Response;
import com.app.address.exception.InvalidAddressException;
import com.app.address.exception.InvalidAddressesException;
import com.app.address.exception.InvalidPincodeException;
import com.app.address.exception.NoAddressFoundException;
import com.app.address.exception.NotBelongCityException;
import com.app.address.microservice.IMicroService;
import com.app.address.repository.AddressRepository;
import com.app.address.util.MessageUtils;

@Service
@Transactional
public class AddressService {

	@Autowired
	AddressRepository addressRepository;
	
	@Autowired
	IMicroService microServiceImpl;
	
	@Autowired
	com.app.address.util.Service utilService;
	
	
	public AddressResponse getAddress(String customerId) throws Exception{
		if (!microServiceImpl.isCustomerAvailable(customerId)) {
			throw new Exception();
		}
		Optional<ArrayList<Address>> addressOptional = addressRepository.findAddressesByCustomerId(customerId);
		if(addressOptional == null || !addressOptional.isPresent()) {
			return new AddressResponse(MessageUtils.NO_ADDRESS_FOUND);
		}
		return new AddressResponse(null, addressOptional.get());
	}

	public Response saveAddressWithGoogleMap(String customerId,String address,String pinCode,String landMark,Double latitude,Double longitude) throws InvalidAddressException, InvalidPincodeException, NotBelongCityException,Exception  {
		if (!microServiceImpl.isCustomerAvailable(customerId)) {
			throw new Exception();
		}
		if(utilService.isNotValidPincode(pinCode)) {
			throw new NotBelongCityException();
		}
		if(microServiceImpl.isPincodeNotBelongToExpectedCity(pinCode)) {
			throw new NotBelongCityException();
		}
		if(address==null || address.isEmpty()) {
			throw new InvalidAddressException();
		}
		if(latitude!=null && longitude!=null) {
			addressRepository.save(new Address(customerId, address, pinCode, new LatLong(latitude, longitude), landMark));
		}
		else {
			addressRepository.save(new Address(customerId, address, pinCode,null,landMark));
		}
		return new Response(MessageUtils.SAVE_ADDRESS_SUCCESSFULLY) {};
	}
	
	public Response saveAddressManually(String customerId,String address,String pinCode,String landMark) throws InvalidAddressException, InvalidPincodeException, NotBelongCityException,Exception  {
		if (!microServiceImpl.isCustomerAvailable(customerId)) {
			throw new Exception();
		}
		if(utilService.isNotValidPincode(pinCode)) {
			throw new Exception();
		}
		if(microServiceImpl.isPincodeNotBelongToExpectedCity(pinCode)) {
			throw new NotBelongCityException();
		}
		if(address==null || address.isEmpty()) {
			throw new InvalidAddressException();
		}
		addressRepository.save(new Address(customerId, address, pinCode,null,landMark));
		return new Response(MessageUtils.SAVE_ADDRESS_SUCCESSFULLY) {};
	}
	
	/*public Response updateAndReturnAddress(Address addressRequest) throws InvalidRequestException, InvalidAddressIdException,
	InvalidCustomerIdException, InvalidAddressException, InvalidPincodeException, NotBelongCityException, SaveAddressException, 
	Exception {
		if(addressRequest==null) {
			throw new InvalidRequestException("updateAndReturnAddress","addressRequest");
		}
		if(addressRequest.getAddressId()==null) {
			throw new InvalidAddressIdException("updateAndReturnAddress");
		}
		if(addressRequest.getCustomerId()==null) {
			throw new InvalidCustomerIdException("updateAndReturnAddress");
		}
		if(addressRequest.getAddress()==null) {
			throw new InvalidAddressException("updateAndReturnAddress");
		}
		if(addressRequest.getAddress().isEmpty()) {
			throw new InvalidAddressException("updateAndReturnAddress");
		}
		if(addressRequest.getPincode()==null) {
			throw new InvalidPincodeException("updateAndReturnAddress");
		}
		if(addressRequest.getPincode().isEmpty()) {
			throw new InvalidPincodeException("updateAndReturnAddress");
		}
		if(addressRequest.getPincode().length()!=6) {
			throw new InvalidPincodeException("updateAndReturnAddress");
		}
		if((boolean) pincodeServiceImpl.get(addressRequest.getPincode())) {
			throw new NotBelongCityException("updateAndReturnAddress");
		}
		Address addressRequestObj = addressRepository.save(addressRequest);
		if(addressRequestObj==null) {
			throw new SaveAddressException();
		}
		return new Response(MessageUtils.UPDATE_ADDRESS_SUCCESSFULLY) {};
	}
	
	public AddressResponse deleteAndReturnAddress(ArrayList<Address> addressRequests) throws InvalidRequestException,
	InvalidAddressesException,InvalidAddressIdException,AddressOptionalNullException,AddressOptionalNotPresentException,
	AddressListNullException,Exception {
		if(addressRequests==null) {
			throw new InvalidRequestException("deleteAndReturnAddress","addressRequests");
		}
		if(addressRequests.isEmpty()) {
			throw new InvalidAddressesException("deleteAndReturnAddress");
		}
		Boolean isAnyAddressIdNull = addressRequests.stream().anyMatch(address->address.getAddressId()==null);
		if(isAnyAddressIdNull) {
			throw new InvalidAddressIdException("deleteAndReturnAddress");
		}
		Long customerId = addressRequests.get(0).getCustomerId();
		addressRepository.deleteAll(addressRequests);
		Optional<ArrayList<Address>> addressesOptional = addressRepository.findAddressesByCustomerId(customerId);
		if(addressesOptional==null) {
			throw new AddressOptionalNullException();
		}
		if(!addressesOptional.isPresent()) {
			throw new AddressOptionalNotPresentException();
		}
		ArrayList<Address> addresses = addressesOptional.get();
		if(addresses==null) {
			throw new AddressListNullException();
		}
		if(addresses.isEmpty()) {
			return new AddressResponse(MessageUtils.NO_ADDRESS_FOUND);
		}
		return new AddressResponse(null,addresses);
	}
	
	public AddressForDeveloperResponse getAddressByCustomerIdAndAddressIdForDeveloper(Long customerId,Long addressId) throws 
				InvalidCustomerIdForDeveloperException,InvalidAddressIdForDeveloperException,AddressOptionalNullForDeveloperException,
				AddressOptionalNotPresentForDeveloperException,AddressNullForDeveloperException,Exception {
		if(customerId==null) {
			throw new InvalidCustomerIdForDeveloperException();
		}
		if(addressId==null) {
			throw new InvalidAddressIdForDeveloperException();
		}
		Optional<Address> addressOptional = addressRepository.findAddressByCustomerIdAndAddressId(customerId,addressId);
		if(addressOptional==null) {
			throw new AddressOptionalNullForDeveloperException();
		}
		if(!addressOptional.isPresent()) {
			throw new AddressOptionalNotPresentForDeveloperException();
		}
		Address address = addressOptional.get();
		if(address==null) {
			throw new AddressNullForDeveloperException();
		}
		return new AddressForDeveloperResponse(null,address);
	}*/
	
}
